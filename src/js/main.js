
//= ../../node_modules/swiper/dist/js/swiper.jquery.js
//= jquery.ddslick.min.js

var Slider = new Swiper('.slider-block', {
    slidesPerView: '1',
    pagination: '.slider-pagination',
    paginationClickable: true,
    spaceBetween: 10,
    autoplay: 2000,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    breakpoints: {
        767: {
            slidesPerView: 'auto'
        }
    }
});
var Slider1 = new Swiper('.swiper1', {
    slidesPerView: 'auto',
    slidesPerColumn: 2,
    spaceBetween: 15,
    loop:false,
    breakpoints: {
        1200: {
            slidesPerColumn: 3
        },
        992: {
            slidesPerColumn: 3
        }
    }
});

$(document).ready(function () {
    var headerHtml = $('.main-header').html();
    $('.smalldisplay').html(headerHtml);
});
$(".toggle-login").on('click', function () {
    $(this).toggleClass('block-hidden').next('.toggle-block').slideToggle();

});

// open feedback modal
function openFeedbackModal() {
    $('#feedback').show();
}

$('#btn_feedback').on('click', function (e) {
    e.preventDefault();
    openFeedbackModal();
});

// close modal
function closeModal(){
    $('#conditions').hide();
    $('#feedback').hide();
    $('#succsess_feedback').hide();
}

$('.close_modal').on('click', function () {
    closeModal();
});

// open Succsess Feedback Modal

function openSuccsessFeedbackModal() {
    $('#feedback').hide();
    $('#succsess_feedback').show();
}

$('.btn_confirm').on('click', function (e) {
    e.preventDefault();
    openSuccsessFeedbackModal();
});

// open Terms and Conditions

function openConditionsModal() {
    $('#conditions').show();
}

$("#registration-form-terms-and-conditions-text").on('click', function () {
    openConditionsModal();
});

// open After Registration block
function openAfterRegistration() {
    $('.after-registration').show().css('display','flex');
}

$("#registration-form-submit-button").on('click', function (e) {
    e.preventDefault();
    openAfterRegistration();
});
$('#myDropdown').ddslick({
    onSelected: function(selectedData){
    }
});